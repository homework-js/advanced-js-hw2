// - Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// - На сторінці повинен знаходитись `div` з `id="root"`, куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// - Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// - Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.
const booksFilter = ['author','name','price'] 

const books = [

    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

  let div = document.createElement('div')
  div.id = 'root'
  document.body.prepend(div)
let ul = document.createElement('ul')
document.getElementById('root').append(ul) 
let i=0
for (const book of books) {
  try {
    i++
    booksCheck(book, booksFilter)
        const li = document.createElement('li');
    li.innerHTML = Object.entries(book)
      .map(([key, value]) => `${key}: ${value}`)
      .join(', ');
    ul.appendChild(li);
    // console.log(book)
  } catch (e) {
    console.error(e.message);
  }
}

function booksCheck(book,booksFilter) {
  for (const key of booksFilter) {
    if (book[key] === undefined) {
      throw new Error(`В обьекте ${i} отсутствует свойство ${key}`);
    }
    }
}





